script_path=$(readlink -f "$0")
script_dir="$(dirname "$script_path")"
home_dir=$(dirname "$(dirname "$(dirname "$script_path")")")
cd "$home_dir" || exit

python3 ./od/model_main.py \
	--checkpoint_dir="$home_dir/models/cctv3" \
	--model_dir="$home_dir/train" \
	--pipeline_config_path="$script_dir/pipeline.config"
