#!/bin/sh

# Count number of images in instance.
jq '.images | length' <"$1"
