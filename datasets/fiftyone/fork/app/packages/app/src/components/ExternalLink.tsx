import React from "react";
import styled from "styled-components";

const Link = styled.a`
  color: ${({ theme }) => theme.font};
`;

const ExternalLink = ({ href, ...props }) => {
  return <Link {...props} href={href} target="_blank" />;
};

export default ExternalLink;
