#!/bin/sh

set -o errexit
set -o xtrace

# Downloads a file with curl, resuming if it's already partially downloaded.
dl() {
    curl --continue-at - --remote-name "$@"
}

script_dir=$(dirname "$(readlink -f "$0")")

cd "$script_dir"

mkdir -p ./coco2017/images

cd "$script_dir/coco2017"

dl http://images.cocodataset.org/annotations/annotations_trainval2017.zip

unzip -nj annotations_trainval2017.zip

dl http://images.cocodataset.org/zips/train2017.zip
dl http://images.cocodataset.org/zips/val2017.zip

unzip -njd images train2017.zip
unzip -njd images val2017.zip
