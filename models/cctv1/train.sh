#!/bin/sh

script_path=$(readlink -f "$0")
script_dir="$(dirname "$script_path")"
home_dir=$(dirname "$(dirname "$(dirname "$script_path")")")
cd "$home_dir" || exit

python3 ../object_detection/model_main.py \
	--run_once \
	--model_dir="$script_dir" \
	--pipeline_config_path="$script_dir/pipeline.config"
