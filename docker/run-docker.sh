#!/bin/sh

script_dir=$(dirname "$(readlink -f "$0")")
home_dir="$script_dir/.."

docker run \
	-p 0.0.0.0:6006:6006 \
	-p 5151:5151 \
	-p 3000:3000 \
	-it -v "$home_dir/:/home/tensorflow/models/research/content" \
	tf1-cctv /bin/bash
