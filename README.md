## TF-CCTV

TF-CCTV is a [Coral](https://coral.ai) compatible [Tensorflow](https://www.tensorflow.org) model designed to process CCTV footage. Originally made for [OS-NVR](https://gitlab.com/osnvr/os-nvr)

The biggest current limitation is the large amount of inaccurately annotated images in the datasets. Over half of the images are either completely wrong or irrelevant for this purpose. We need your help to [sort the images.](./docs/sorting_images.md)

[![Watch the video](https://img.youtube.com/vi/sQ4MTpRLf-k/hqdefault.jpg)](https://www.youtube.com/watch?v=sQ4MTpRLf-k)

[Matrix chat](https://matrix.to/#/#tf-cctv:matrix.org)


<br>

#### Drop-in Docker replacements.

[Frigate](https://hub.docker.com/r/curid/frigate_tf-cctv)

[DOODS2](https://hub.docker.com/r/curid/doods2_tf-cctv)

<br>

### Models

The models are based on MobileDet with additional training data added.


##### CCTV1

Proof of concept model. Can only detect `person`. Resolution changed from `320x320` to `420x280`. Achieves an improvement of 1.7mAP on gray images. Though at the cost of approximately 18% slower inference time.

<details>
<summary>CCTV2 failed run</summary>
Frigate compatible model. Can only detect `person`. Resolution changed from `320x320` to `340x340`. Approximately 18% slower inference time. Failed run, accuracy is worse than original model.
</details>

##### CCTV3

Frigate compatible model. Only trained on gray images. Can only detect `person`. Resolution changed from `320x320` to `340x340`. Achieves the same accuracy on both gray and color images. Approximately 18% slower inference time. 

<br>

### BENCHMARKS

|      | MODEL          | TESTSET                    | mAP  |
| ---- | -------------- | -------------------------- | ---- |
|      | mobiledet      | color\_coco-minival_person | 44.1 |
|      | mobiledet      | gray\_coco-minival_person  | 38.9 |
|      | -              |                            |      |
|      | rcnn_inception | color\_coco-minival_person | 50.7 |
|      | rcnn_inception | gray\_coco-minival_person  | 45.8 |
|      | -              |                            |      |
| r1.0 | cctv1          | color\_coco-minival_person | 43.7 |
| r1.0 | cctv1          | gray\_coco-minival_person  | 40.6 |
|      | -              |                            |      |
| r1.1 | cctv2          | color\_coco-minival_person | 39.1 |
| r1.1 | cctv2          | gray\_coco-minival_person  | 37   |
|      | -              |                            |      |
| r1.2 | cctv3          | color\_coco-minival_person | 39   |
| r1.2 | cctv3          | gray\_coco-minival_person  | 39.1 |
|      | -              |                            |      |
| r1.3 | cctv3.1        | color\_coco-minival_person | 42.1 |
| r1.3 | cctv3.1        | gray\_coco-minival_person  | 42.9 |
|      | -              |                            |      |
| r1.4 | cctv3.2        | color\_coco-minival_person | 43.2 |
| r1.4 | cctv3.2        | gray\_coco-minival_person  | 43.1 |
|      | -              |                            |      |
| r1.5 | cctv3.3        | color\_coco-minival_person | 43.4 |
| r1.5 | cctv3.3        | gray\_coco-minival_person  | 43.4 |

<br>

### BASE MODELS

| SIZE    | NAME           | FULL NAME                         |
| ------- | -------------- | --------------------------------- |
| 320x320 | mobiledet      | ssdlite\_mobiledet\_edgetpu/fp32  |
| any     | rcnn_inception | faster\_rcnn\_inception\_v2\_coco |

<br>

### TESTSETS

Blacklisted images are removed from all testsets.

| IMAGES | NAME                        | DESCRIPTION                                         |
| ------ | ----------------------------|-----------------------------------------------------|
| 720    | coco-minival_person         | Minival color and gray, person only.                |
| 349    | color\_coco-minival_person  | Minival original, cctv1 labels. Gray images removed.|
| 371    | gray\_coco-minival_person   | Minival coverted to black and white, person only.   |

<br>

### TRAINSETS

| SIZE | NAME    | DESCRIPTION                                                             |
| ---- | ------- | ------------------------------------------------------------------------|
| 144k | cctv1   | person only. Combination of coco2017 and voc2012. Blacklist not applied |
| 144k | cctv2   | Same as cctv1.                                                          |
| 59k  | cctv3   | person only. coco2017 and voc2012 images coverted to gray.              |
| 54k  | cctv3.1 | The first 100k person IDs sorted.                                       |
| 50k  | cctv3.2 | The first 200k person IDs sorted.                                       |
| 44k  | cctv3.3 | The first 300k person IDs sorted.                                       |


<br>

#### Naming convention.

\<TYPE>\_\<DATASET>\_\<LABELS>

| type  | description                            |
|-------|----------------------------------------|
| color | Original images.                       |
| gray  | Images converted to black and white.   |
| empty | Combined color and gray.               |

<br>

### DATASETS

#### COCO2017

https://cocodataset.org

#### VOC2012

http://host.robots.ox.ac.uk/pascal/VOC/voc2012/

<br>

### Resources

[https://coral.ai/docs/edgetpu/retrain-detection](https://coral.ai/docs/edgetpu/retrain-detection)

[Custom object detection from scratch](https://towardsdatascience.com/custom-object-detection-using-tensorflow-from-scratch-e61da2e10087)

[Effect of batch size on training dynamics](https://medium.com/mini-distill/effect-of-batch-size-on-training-dynamics-21c14f7a716e)

[MobileNetV2: Inverted Residuals and Linear Bottlenecks](https://arxiv.org/pdf/1801.04381.pdf)

[https://elitedatascience.com/category/explainers](https://elitedatascience.com/category/explainers)

<br>


Copyright (C) 2021-2023 Curid, <Curid@protonmail.com>
