#!/bin/sh

if [ "$#" -ne 1 ]; then
	echo "Need tflite model to convert."
	exit 1
fi

model_path=$(realpath "$1")
model_dir=$(dirname "$model_path")

cd "$model_dir" || exit

edgetpu_compiler -s "$model_path"
