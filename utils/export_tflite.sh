#!/bin/sh

if [ "$#" -ne 1 ]; then
	echo "Need checkpoint prefix to export."
	exit 1
fi

script_dir=$(dirname "$(readlink -f "$0")")
home_dir="$script_dir/.."
tmp_dir="$home_dir/tmp"

checkpoint_path=$(realpath "$1")
checkpoint_dir=$(dirname "$checkpoint_path")

cd "$home_dir" || exit

python3 ./od/export_tflite_ssd_graph.py \
	--pipeline_config_path="$checkpoint_dir/pipeline.config" \
	--trained_checkpoint_prefix="$checkpoint_path" \
	--output_directory="$checkpoint_dir" \
	--add_postprocessing_op=true
