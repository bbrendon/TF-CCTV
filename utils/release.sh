#!/bin/sh

# OUTPUT.
#
# TF-CCTV
# ├── cctv1
# │   ├── cctv1_420x280_edgetpu.tflite
# │   ├── cctv1_420x280.tflite
# │   ├── checkpoint
# │   ├── labels-doods.txt
# │   ├── labels.pbtxt
# │   ├── model.ckpt-60000.data-00000-of-00001
# │   ├── model.ckpt-60000.index
# │   ├── model.ckpt-60000.meta
# │   ├── pipeline.config
# │   ├── tflite_graph.pb
# │   └── tflite_graph.pbtxt
# └── cctv2
#     ├── cctv2_340x340_edgetpu.tflite
#     ├── cctv2_340x340.tflite
#     ├── checkpoint
#     ├── labels-doods.txt
#     ├── labels.pbtxt
#     ├── model.ckpt-60000.data-00000-of-00001
#     ├── model.ckpt-60000.index
#     ├── model.ckpt-60000.meta
#     ├── pipeline.config
#     ├── tflite_graph.pb
#     └── tflite_graph.pbtxt

if [ "$#" -ne 1 ]; then
	echo "Need release version."
	exit 1
fi

release_ver=$1

script_dir=$(dirname "$(readlink -f "$0")")
home_dir="$script_dir"/..
release_dir="$home_dir"/TF-CCTV
cctv1_release_dir="$release_dir"/cctv1/
cctv2_release_dir="$release_dir"/cctv2/

cd "$home_dir" || exit

mkdir -p "$cctv1_release_dir"
cp "$home_dir"/models/cctv1/checkpoint "$cctv1_release_dir"
cp "$home_dir"/models/cctv1/pipeline.config "$cctv1_release_dir"
cp "$home_dir"/models/cctv1/tflite_graph* "$cctv1_release_dir"
cp "$home_dir"/models/cctv1/model.ckpt* "$cctv1_release_dir"
cp "$home_dir"/models/cctv1/cctv1_420x280*.tflite* "$cctv1_release_dir"
cp "$home_dir"/labels/cctv1.pbtxt "$cctv1_release_dir"/labels.pbtxt
cp "$home_dir"/labels/cctv1-doods.txt "$cctv1_release_dir"/labels-doods.txt

mkdir "$cctv2_release_dir"
cp "$home_dir"/models/cctv2/checkpoint "$cctv2_release_dir"
cp "$home_dir"/models/cctv2/pipeline.config "$cctv2_release_dir"
cp "$home_dir"/models/cctv2/tflite_graph* "$cctv2_release_dir"
cp "$home_dir"/models/cctv2/model.ckpt* "$cctv2_release_dir"
cp "$home_dir"/models/cctv2/cctv2_340x340*.tflite* "$cctv2_release_dir"
cp "$home_dir"/labels/cctv2.pbtxt "$cctv2_release_dir"/labels.pbtxt
cp "$home_dir"/labels/cctv2-doods.txt "$cctv2_release_dir"/labels-doods.txt

tar -czf TF-CCTV_"$release_ver".tar.gz "$release_dir"

rm -r "$release_dir"
