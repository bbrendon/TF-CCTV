

### How to contribute

All images should be larger than `640x480`.

Only contribute images that you own. Do not use copyrighted videos from Youtube.

Open a new [issue]() and upload the images. Use separate [Issues]() for positive and negative examples.

Images will then be reviewed, and if accepted added to the repository. They will be used in the next training run and release.

By uploading images, you allow them to be licensed under the project [license](../LICENSE).

<br>

### Positive examples

Positive examples tells the model what is an object. These examples will need to have their objects marked manually.

We already have 72k images with positive examples of `person`. But new unique examples could still be useful.

Any footage of actual burglars is also welcome.

<br>

### Negative examples

Negative examples that tells the model what's not an object. Helps to reduce false positives.

Negative images cannot contain humans, animals or vehicles.

Images should be unique, for each camera you could take snapshots at different times of the day.

* noon
* midnight
* morning shadows
* evening shadows

And different weathers.

* sunny
* cloudy
* raining

