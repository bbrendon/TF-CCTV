

## USAGE

Instruction for how to use the included scrips.

Its recommended to run all commands from inside the included Docker.

* [Downloading the datasets](#downloading_the_datasets)
* [Generating gray datasets](#generating_gray_datasets)
* [Generating tfrecords](#generating_tfrecords)
* [Running evaluation](#running_evaluation)
* [Training a model](#training_a_model)

<br>

## Downloading the datasets

Download COCO2017 (20GB)

`./datasets/coco2017.sh`

Download VOC2012 (2GB)

`./datasets/voc2012.sh`

The datasets directory should look like this afterwards.

```
datasets
├── coco2017
│   ├── annotations_trainval2017.zip
│   ├── images                        // 123287 images in a single directory.
│   ├── instances_train2017.json
│   └── instances_val2017.json
└── VOC2012
    ├── Annotations  // 17125 files.
    ├── ImageSets
    │   ├── Action
    │   ├── Layout
    │   ├── Main
    │   └── Segmentation
    ├── JPEGImages         // 17125 images.
    ├── SegmentationClass
    └── SegmentationObject
```


## Generating gray datasets

```
./datasets/gen_gray-coco.sh
./datasets/gen_gray-voc.sh
```


The datasets directory should look like this afterwards.

```
datasets
├── coco2017
│   ├── annotations_trainval2017.zip
│   ├── images                        // 123287 images in a single directory.
│   ├── instances_train2017.json
│   └── instances_val2017.json
├── gray-coco2017
│   ├── images                    // 123287 images in a single directory.
│   ├── instances_train2017.json
│   └── instances_val2017.json
├── gray-VOC2012
│   ├── Annotations // 17125 files.
│   └── JPEGImages  // 17125 images.
└── VOC2012
    ├── Annotations  // 17125 files.
    ├── ImageSets
    │   ├── Action
    │   ├── Layout
    │   ├── Main
    │   └── Segmentation
    ├── JPEGImages         // 17125 images.
    ├── SegmentationClass
    └── SegmentationObject
```

<br>


## Generating tfrecords

Generating tfrecords from the datasets.

Each records directory has a `gen_records.py` file.

`python3 ./records/gray_coco-minival_person/gen_records.py

<br>


## Running evaluation

Assuming you've generated tfrecords for the testset you wish to use.

You can evaluate the cctv1 model with the `coco-minival_cctv1` testset using.

`./eval/cctv1--gray_coco-minival_person/eval.sh `


<br>


## Training a model

If you're familiar with the Tensorflow 1 object detection api, you can find the pipeline.config in the models directory.

Otherwise i suggest opening a issue.