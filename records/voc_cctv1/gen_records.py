import os

from py_utils.create_records import create_records
from py_utils.processor import Processor
from py_utils.processor import combine_instances
from py_utils.utils import check_sum


dir_path = os.path.dirname(os.path.realpath(__file__))
home_path = os.path.dirname(os.path.dirname(dir_path))
datasets_path = os.path.join(home_path, "datasets")
labels_path = os.path.join(home_path, "labels", "cctv1.pbtxt")

p = Processor(datasets_path, labels_path)

color_voc = p.gen_voc(3)
gray_voc = p.gen_voc(4, gray=True)

instance = combine_instances([color_voc, gray_voc])

create_records(instance, dir_path, 100)
check_sum(instance, dir_path)
