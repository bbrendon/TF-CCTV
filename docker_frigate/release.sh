#!/bin/sh

version="0.11.1" # Frigate version.
release="r1.4"   # TF-CCTV release.

script_dir=$(dirname "$(readlink -f "$0")")
cd "$script_dir" || exit

../utils/prepare_models.sh "$script_dir"


docker buildx build \
	--pull --push \
	--build-arg version="$version" \
	-t curid/frigate_tf-cctv:"$version"_"$release" \
	--platform linux/amd64,linux/arm,linux/arm64 .
