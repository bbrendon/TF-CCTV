#!/bin/sh

version="0.11.1" # Frigate version.

script_dir=$(dirname "$(readlink -f "$0")")
cd "$script_dir" || exit

../utils/prepare_models.sh "$script_dir"

docker image build \
	--build-arg version="$version" \
	-t curid/frigate_tf-cctv:local .
